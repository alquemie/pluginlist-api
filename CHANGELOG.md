# Changelog

Relevant changes to the Alquemie Plugin List project are documented below as a resource for users.

## [0.1.7] - 2019-03-14
### Changed
- Changed Name to prevent conflict with WP.org plugin

## [0.1.7] - 2019-03-14
### Changed
- Moved configuration page into "Settings" menu

## [0.1.6] - 2019-03-14
### Changed
- Made MU Plugin

## [0.1.5] - 2019-03-14
### Added
- API Key Security

### Changed
- Fixed Slug Logic
- Changed Endpoint

## [0.1.0] - 2019-03-14
### Added
- Initial Coding display installed plugin data via REST API 

### Changed
- N/A
