<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'AlquemiePL_REST_Controller' ) ) {
	class AlquemiePL_REST_Controller extends WP_REST_Controller {

        /**
         * @var string
         */
		protected static $token = null;
		

		public function __construct( $type = null ) {

            self::$token = get_option('alquemie_pluginlist_sitekey', null);     
            // parent::__construct();
		}

		public function get_item_permissions_check( $request ) {
			$key = $request->get_header('aqpl-key');
			if (!empty($key)) {
				// error_log($key);
				if ($key == self::$token) {
					return true;
				} 
			} elseif (self::$token == '') {
				return true;
			}

			return false;  // should be false
		}

		public function register_routes() {
			register_rest_route( 'plugins/v1', '/list/?', array(
				array(
					'methods'             => WP_REST_Server::READABLE,
					'callback'            => array( $this, 'get_plugins' ),
					'permission_callback' => array( $this, 'get_item_permissions_check' ),
				),
			) );

		}

		public function register() {
			$this->register_routes();
		}

		
		public function get_plugins( $request ) {
			if ( ! function_exists( 'get_plugins' ) ) {
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
			}
			
			$all_plugins = get_plugins();
			$callback = function($key, $value) {
				$parts = explode( DIRECTORY_SEPARATOR, $key );
				return array('slug' => $parts[0], 'name' => $value['Name'], 'version' => $value['Version']);
			};
			 
			$my_plugins = array_map($callback, array_keys($all_plugins), $all_plugins);

			return $my_plugins;
		}


	}
}
