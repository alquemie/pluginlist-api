<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if (!class_exists('AlquemiePL_AdminSettings')) :
	/* TwoX Info Settings Settings Page */
	class AlquemiePL_AdminSettings {

		public function __construct() {
			add_action( 'admin_menu', array( $this, 'create_settings' ) );
			add_action( 'admin_init', array( $this, 'setup_sections' ) );
			add_action( 'admin_init', array( $this, 'setup_fields' ) );
		}
		public function create_settings() {
			$page_title = 'Plugin List';
			$menu_title = 'Plugin List';
			$capability = 'manage_options';
			$slug = 'alquemie-pluginlist';
			$callback = array($this, 'settings_content');
			$icon = 'dashicons-id-alt';
			$position = 2;
			//add_menu_page($page_title, $menu_title, $capability, $slug, $callback, $icon, $position);
			add_options_page($page_title, $menu_title, $capability, $slug, $callback);
		}
		public function settings_content() { ?>
			<div class="wrap">
				<h1>Alquemie Plugin List Settings</h1>
				<?php settings_errors(); ?>
				<form method="POST" action="options.php">
					<?php
						settings_fields( 'alquemie-pluginlist' );
						do_settings_sections( 'alquemie-pluginlist' );
						submit_button();
					?>
				</form>
			</div> <?php
		}

		public function setup_sections() {
			add_settings_section( 'alquemie_pluginlist_section', 'Configuration settings for Pluglin List API', array(), 'alquemie-pluginlist' );
			
			$update = (isset($_GET['settings-updated'])) ? $_GET['settings-updated'] : false;
	
		}

		public function setup_fields() {
			$fields = array(
				array(
					'label' => 'Site Key',
					'id' => 'alquemie_pluginlist_sitekey',
					'type' => 'text',
					'section' => 'alquemie_pluginlist_section',
					'desc' => '',
					'placeholder' => ''
				),

		
			);
			foreach( $fields as $field ){
				add_settings_field( $field['id'], $field['label'], array( $this, 'field_callback' ), 'alquemie-pluginlist', $field['section'], $field );
				register_setting( 'alquemie-pluginlist', $field['id'] );
			}
		}

		public function field_callback( $field ) {
			$value = get_option( $field['id'] );
			switch ( $field['type'] ) {
					case 'select':
					case 'multiselect':
						if( ! empty ( $field['options'] ) && is_array( $field['options'] ) ) {
							$attr = '';
							$options = '';
							foreach( $field['options'] as $key => $label ) {
								$options.= sprintf('<option value="%s" %s>%s</option>',
									$key,
									selected($value[array_search($key, $value, true)], $key, false),
									$label
								);
							}
							if( $field['type'] === 'multiselect' ){
								$attr = ' multiple="multiple" ';
							}
							printf( '<select name="%1$s[]" id="%1$s" %2$s>%3$s</select>',
								$field['id'],
								$attr,
								$options
							);
						}
						break;
				default:
					printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" size="35" />',
						$field['id'],
						$field['type'],
						$field['placeholder'],
						$value
					);
			}
			if( $desc = $field['desc'] ) {
				printf( '<p class="description">%s </p>', $desc );
			}
		}

		
	}

	new AlquemiePL_AdminSettings();

endif;