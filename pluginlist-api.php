<?php
/*
Plugin Name: Alquemie Plugin List
Description: Expose installed plugins via REST API
Version: 0.1.8
Author: Chris Carrel
Author URI: https://www.linkedin.com/in/chriscarrel
License:     GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

------------------------------------------------------------------------

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


if ( !defined('ABSPATH') ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die;
}

require_once dirname( __FILE__ ) . '/lib/autoload.php';

if (!class_exists('Alquemie_PluginList')) :
	/**
	 *
	 */
	class Alquemie_PluginList {
	
		public static function init() {
			self::includes();
			self::hooks();
		}

		private static function includes() {
			require_once dirname( __FILE__ ) . '/admin/class-pluginlist-admin.php';
			require_once dirname( __FILE__ ) . '/endpoints/class-pluginlist-controller.php';
		}

		private static function hooks() {
			add_action( 'rest_api_init', array( __CLASS__, 'create_rest_routes' ), 10 );
		}

		public static function create_rest_routes() {
			$controller = new AlquemiePL_REST_Controller;
			$controller->register();
		}

	}

	add_action( 'plugins_loaded', array( 'Alquemie_PluginList', 'init' ) );
	
endif;
